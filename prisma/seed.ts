import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();
async function main() {
  const glaucia = await prisma.user.upsert({
    where: { email: 'glaucia@ufv.br' },
    update: {},
    create: {
      name: 'Gláucia Braga e Silva',
      email: 'glaucia@ufv.br',
      password: '$2a$10$k2rXCFgdmO84Vhkyb6trJ.oH6MYLf141uTPf81w04BImKVqDbBivi', // random42
      cpf: '82501862066',
      role: 'COORDINATOR',
    },
  });
  console.log({ glaucia });
}
main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
