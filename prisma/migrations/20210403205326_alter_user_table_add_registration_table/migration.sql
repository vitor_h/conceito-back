/*
  Warnings:

  - You are about to drop the column `registration` on the `User` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "User.registration_unique";

-- AlterTable
ALTER TABLE "User" DROP COLUMN "registration";

-- CreateTable
CREATE TABLE "Registration" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(20) NOT NULL,
    "userId" INTEGER NOT NULL,

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Registration.name_unique" ON "Registration"("name");

-- CreateIndex
CREATE UNIQUE INDEX "Registration_userId_unique" ON "Registration"("userId");

-- AddForeignKey
ALTER TABLE "Registration" ADD FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
