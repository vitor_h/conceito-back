-- CreateEnum
CREATE TYPE "UserType" AS ENUM ('TEACHER', 'COORDINATOR', 'STUDENT');

-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(200) NOT NULL,
    "password" VARCHAR(20) NOT NULL,
    "cpf" VARCHAR(11) NOT NULL,
    "registration" VARCHAR(20),
    "role" "UserType" NOT NULL DEFAULT E'STUDENT',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "deletedAt" TIMESTAMP(3),

    PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User.email_unique" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "User.cpf_unique" ON "User"("cpf");

-- CreateIndex
CREATE UNIQUE INDEX "User.registration_unique" ON "User"("registration");
