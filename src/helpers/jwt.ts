/* eslint-disable @typescript-eslint/no-non-null-assertion */
import jwt from 'jsonwebtoken';
import APIError from '../config/error';
import { PrismaClient, User } from '@prisma/client';

const { JWT_SECRET, BASE_NAME } = process.env;

/**
 * Extracts the JWT in authorization header
 */
function getTokenFromRequest(req): string | null {
  const authorization = req.header('authorization');
  if (!authorization) return null;

  return authorization.split(' ')[1]; // Bearer
}

/**
 * Extracts the resource data on the JWT in authorization header
 */
function getPayload(req) {
  const token = getTokenFromRequest(req);
  return token && jwt.decode(token);
}

/**
 * Extracts the resource id on the JWT in authorization header
 */
function getResourceId(req) {
  const data = getPayload(req);
  return data && data.sub;
}

/**
 * Extracts the user|client data on the JWT in authorization header
 */
async function getResource(req, prisma: PrismaClient) {
  const payload = getPayload(req);
  const id = getResourceId(req);
  if (!payload || !payload['data']) return { user: null };

  if (payload['data'].type === 'user') {
    const user = await prisma.user.findFirst({
      where: {
        id,
      },
    });
    return { user: { id, ...user } };
  }

  return { user: null };
}

/**
 * Generates a JWT
 * @param  {User} user User Model instance
 */
function generateUserToken(user: User) {
  const { id, name, role } = user;
  const payload = {
    sub: id,
    data: { name, role },
  };

  const issuer =
    (BASE_NAME || 'calendar').toUpperCase().split(' ').join('_') + '_API';
  const options = {
    expiresIn: '50365d',
    issuer,
  };

  return jwt.sign(payload, JWT_SECRET!, options);
}

/**
 * Checks if the token in Authorization header is present and valid
 */
async function middleware(req, _res, next) {
  const token = getTokenFromRequest(req);

  if (!token || !(token.length > 0)) throw new APIError('no_token', 401);

  try {
    await jwt.verify(token, JWT_SECRET!);
    next();
  } catch (e) {
    if (e.name === 'TokenExpiredError')
      throw new APIError('token_expired', 403);
    throw new APIError(e.message.split(' ').join('_').toLowerCase(), 401);
  }
}

export {
  getPayload,
  getResourceId,
  getResource,
  generateUserToken,
  getTokenFromRequest,
  middleware,
};
