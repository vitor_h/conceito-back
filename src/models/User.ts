import { objectType, enumType } from '@nexus/schema';
export const UserType = enumType({
  name: 'UserType',
  description: 'Role of the user',
  members: ['TEACHER', 'COORDINATOR', 'STUDENT'],
});
export const User = objectType({
  name: 'User',
  definition(t) {
    t.model.id({ description: 'ID of the user' });
    t.model.name({ description: 'Name of the user' });
    t.model.email({ description: 'E-mail of the user' });
    t.model.cpf({ description: 'Document identification of the user' });
    t.field('registration', {
      type: 'String',
      description: 'Registration for this user if exists',
      resolve: async (parent, _params, context) => {
        const registration = await context.prisma.registration.findFirst({
          where: {
            userId: parent.id,
          },
        });
        if (!registration) return null;
        return registration.name;
      },
    });
    t.field('role', {
      type: 'UserType',
      description: 'Role of the user',
    });
    t.model.createdAt({ description: 'Creation of the user' });
    t.model.updatedAt({ description: 'Update of the user' });
    t.model.deletedAt({ description: 'Deletion of the user' });
  },
});
