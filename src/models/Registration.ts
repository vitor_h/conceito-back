import { objectType } from '@nexus/schema';
export const Registration = objectType({
  name: 'Registration',
  definition(t) {
    t.model.id({ description: 'ID of the registration' });
    t.model.name({ description: 'String registration' });
    t.model.user({
      description: 'User for this registration',
    });
    t.model.userId({
      description: 'ID for user of this registation',
    });
  },
});
