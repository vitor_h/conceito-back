import { applyMiddleware } from 'graphql-middleware';
import { nexusSchemaPrisma } from 'nexus-plugin-prisma/schema';
import { makeSchema, asNexusMethod } from 'nexus';
import { GraphQLDateTime } from 'graphql-iso-date';
import * as types from '../modules';

export const DateTime = asNexusMethod(GraphQLDateTime, 'date');

const schemaWithoutPermissions = makeSchema({
  types,
  plugins: [
    nexusSchemaPrisma({
      experimentalCRUD: true,
      scalars: {
        DateTime,
      },
    }),
  ],
  outputs: {
    schema: `${__dirname}/../generated/schema.graphql`,
    typegen: `${__dirname}/../generated/types/graphql.ts`,
  },
  contextType: {
    module: require.resolve('./context'),
    export: 'Context',
  },
  sourceTypes: {
    modules: [
      {
        module: '@prisma/client',
        alias: 'prisma',
      },
    ],
  },
});

export const schema = applyMiddleware(schemaWithoutPermissions);
