import { Express } from 'express';
import { createServer } from 'http';
import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import { schema } from './schema';
import { createContext } from './context';

const { PORT: port } = process.env;
const start = (app: Express) => {
  const server = new ApolloServer({
    schema,
    introspection: true,
    playground: {
      endpoint: 'playground',
    },
    context: createContext,
  });
  app.use('*', cors());
  server.applyMiddleware({ app, path: '/api' });
  const httpServer = createServer(app);
  server.installSubscriptionHandlers(httpServer);
  httpServer.listen({ port }, (): void => {
    console.log(
      `\n🚀 GraphQL API is now running on http://0.0.0.0:${port}/api`,
    );
    console.log(
      `\n⏰ Subscriptions ready at ws://0.0.0.0:${port}${server.subscriptionsPath}`,
    );
  });
};

export { start };
