/* eslint-disable @typescript-eslint/ban-types */
/**
 * @author Gustavo F. Viegas
 */
class APIError extends Error {
  status: number;

  constructor(message: string | object = 'Unhandled error', status = 500) {
    const msg =
      (typeof message === 'string' && message) || JSON.stringify(message);

    super(msg);
    this.name = 'APIError';
    this.status = status;
    this.message = msg;
  }
}

export default APIError;
