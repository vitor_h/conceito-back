import { PrismaClient, User } from '@prisma/client';
import { PubSub } from 'apollo-server';
import { getResource } from '../helpers/jwt';

const prisma = new PrismaClient();
const pubsub = new PubSub();

export interface Context {
  prisma: PrismaClient;
  pubsub: PubSub;
  req: any; // HTTP request carrying the `Authorization` header
  authedUser: User;
}

const createContext = async ({ req, ...ctx }) => {
  const { user: authedUser } = await getResource(req, prisma);
  return {
    req,
    ...ctx,
    prisma,
    pubsub,
    authedUser,
  };
};

export { prisma, pubsub, createContext };
