import APIError from '../../config/error';
import { Context } from '../../config/context';
import { compare } from 'bcryptjs';
import { generateUserToken } from '../../helpers/jwt';
import { AuthenticationError } from 'apollo-server-express';

const signInUser = async (context: Context, { cpf, password }) => {
  const user = await context.prisma.user.findUnique({
    where: {
      cpf,
    },
  });
  if (!user) throw new APIError('not_found', 404);

  const passwordValid = await compare(password, user.password);
  if (!passwordValid) {
    throw new APIError('wrong_credentials', 422);
  }

  const token = generateUserToken(user);
  return { token, user };
};

const authenticatedUser = async (context: Context) => {
  if (!context.authedUser) throw new AuthenticationError('no_token');
  return context.authedUser;
};

export { signInUser, authenticatedUser };
