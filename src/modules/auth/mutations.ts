import { extendType, nonNull, stringArg } from '@nexus/schema';
import { signInUser } from './controller';

export const AuthLoginMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('login', {
      type: 'AuthenticatedUser',
      description: 'Login user',
      args: {
        cpf: nonNull(
          stringArg({
            description: 'User cpf',
          }),
        ),
        password: nonNull(
          stringArg({
            description: 'User password',
          }),
        ),
      },
      resolve: (_parent, params, context) => signInUser(context, params),
    });
  },
});
