import { objectType } from '@nexus/schema';

export * from './queries';
export * from './mutations';

export const AuthenticatedUser = objectType({
  name: 'AuthenticatedUser',
  definition(t) {
    t.string('token', { description: 'JWT Token for user authenticated' });
    t.field('user', { type: 'User', description: 'User authenticated' });
  },
});
