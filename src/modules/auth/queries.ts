import { extendType } from '@nexus/schema';
import { authenticatedUser } from './controller';

export const AuthMeQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('me', {
      type: 'User',
      description: 'Authenticated user',
      resolve: (_parent, _params, context) => authenticatedUser(context),
    });
  },
});
