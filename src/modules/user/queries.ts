import { queryType } from '@nexus/schema';

export const UsersQuery = queryType({
  definition(t) {
    t.crud.users({
      description: 'Fetched users',
      pagination: true,
      ordering: true,
      filtering: true,
    });
  },
});
